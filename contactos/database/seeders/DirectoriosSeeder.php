<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DirectoriosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directorios')->insert([
            [
                'nombre_completo'=> 'Saul Mamani m',
                'direccion'=>'plan 500, ororu',
                'telefono'=> '3113',
                'foto' => null
            ],
            [
                'nombre_completo'=> 'jhon matoma',
                'direccion'=>'villavicencio',
                'telefono'=> '3118',
                'foto' => null
            ],
        ]);
    }
}
